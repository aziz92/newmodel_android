package com.example.myapplication

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import com.example.myapplication.databinding.ActivityMainBinding
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import android.content.res.AssetFileDescriptor
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.tensorflow.lite.DataType
import java.nio.ByteBuffer
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.common.TensorOperator
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import java.nio.ByteOrder


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var interpreter: Interpreter

    private var imageLength = 224

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .setAnchorView(R.id.fab).show()
        }
        try {
            interpreter = Interpreter(loadModelFile())
            inspectModel(interpreter)

            val bitmap = getBitmapFromDrawable(this, R.drawable.real2)
            val buffer = convertBitmapToByteBuffer(bitmap)
            val output = Array(1) { ByteArray(3) } // Shape [1, 3] to match the output shape of the model
            interpreter.run(buffer, output)
            output.forEach { byteArray ->
                println(byteArray.joinToString(", ") { byte -> (byte.toInt() and 0xFF).toString() })
            }



//            println("Getting bitmap")
//            val bitmap = getBitmapFromDrawable(this, R.drawable.full3)
//            println("Converting to tensor / normalizing")
//            val tensorImage = loadAndProcessImage(bitmap)
//            println("Transposing")
////            val buffer = transposeAndAddBatchDimension(tensorImage)
//            println("Inferring")
//            runInference(tensorImage.buffer)
//            println("Done")

        } catch (e: Exception) {
            e.printStackTrace()
            println("EXCEPTION RAISED")
            println(e.message)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    private fun loadModelFile(): MappedByteBuffer {
        val assetFileDescriptor: AssetFileDescriptor = this.assets.openFd("large.tflite")
        val fileInputStream = FileInputStream(assetFileDescriptor.fileDescriptor)
        val fileChannel = fileInputStream.channel
        val startOffset = assetFileDescriptor.startOffset
        val declaredLength = assetFileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    private fun inspectModel(interpreter: Interpreter) {
        val inputCount = interpreter.inputTensorCount
        println("Number of input tensors: $inputCount")

//        for (i in 0 until inputCount) {
//            val inputShape = interpreter.getInputTensor(0).shape().clone()  // Get the current input shape
//            inputShape[1] = imageLength  // Modify the height
//            inputShape[2] = imageLength   // Modify the width
//
//            interpreter.resizeInput(0, inputShape)  // Resize the input tensor
//            interpreter.allocateTensors()
//        }

        for (i in 0 until inputCount) {
            val inputTensor = interpreter.getInputTensor(i)
            println("Input tensor $i information:")
            println(" - Shape: ${inputTensor.shape().contentToString()}")
            println(" - Type: ${inputTensor.dataType()}")
            println(" - Name: ${inputTensor.name()}")
        }

        val outputCount = interpreter.outputTensorCount
        println("Number of output tensors: $outputCount")

        for (i in 0 until outputCount) {
            val outputTensor = interpreter.getOutputTensor(i)
            println("Output tensor $i information:")
            println(" - Shape: ${outputTensor.shape().contentToString()}")
            println(" - Type: ${outputTensor.dataType()}")
            println(" - Name: ${outputTensor.name()}")
        }
    }

    fun classifyImage(imageName: String): ByteArray {
        val bitmap = loadImageFromAssets(imageName)
        val byteBuffer = convertBitmapToByteBuffer(bitmap)
        val result = ByteArray(3)  // Assuming model output size is [1, 3]
        interpreter.run(byteBuffer, result)
        return result
    }

    private fun loadImageFromAssets(imageName: String): Bitmap {
        val inputStream = this.assets.open(imageName)
        return BitmapFactory.decodeStream(inputStream)
    }

    private fun convertBitmapToByteBuffer(bitmap: Bitmap): ByteBuffer {
        val byteBuffer = ByteBuffer.allocateDirect(1 * 224 * 224 * 3)
        byteBuffer.order(ByteOrder.nativeOrder())
        bitmap.reconfigure(224, 224, Bitmap.Config.ARGB_8888)

        val intValues = IntArray(224 * 224)
        bitmap.getPixels(intValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

        for (value in intValues) {
            byteBuffer.put((value shr 16 and 0xFF).toByte()) // Red
            byteBuffer.put((value shr 8 and 0xFF).toByte())  // Green
            byteBuffer.put((value and 0xFF).toByte())         // Blue
        }
        byteBuffer.rewind()  // Rewind the buffer to the beginning so it's ready to be read by the model
        return byteBuffer
    }

    fun loadAndProcessImage(bitmap: Bitmap): TensorImage {
        // Create a TensorImage object
        val tensorImage = TensorImage(DataType.UINT8)

        // Load the bitmap into the TensorImage
        tensorImage.load(bitmap)

        // Create an ImageProcessor with all required transformations
        val imageProcessor = ImageProcessor.Builder()
            .add(ResizeOp(imageLength, imageLength, ResizeOp.ResizeMethod.BILINEAR))
//            .add( NormalizeOp(0f, 255f))
            .build()

        // Process the TensorImage
        return imageProcessor.process(tensorImage)
    }

    fun transposeAndAddBatchDimension(tensorImage: TensorImage): TensorBuffer {
        // Extract the current buffer and shape
        val buffer = tensorImage.tensorBuffer
        val height = tensorImage.height
        val width = tensorImage.width
        val channels = 3  // Assuming RGB

        // Allocate a new buffer for the transposed data
        val size = height * width * channels
        val newBuffer = ByteBuffer.allocateDirect(size * 4)  // Floats are 4 bytes
        newBuffer.order(buffer.buffer.order())  // Match the byte order
        val floatBuffer = newBuffer.asFloatBuffer()

        // Transpose from HWC (Height, Width, Channels) to CHW (Channels, Height, Width)
        val floatArray = buffer.floatArray
        for (c in 0 until channels) {
            for (h in 0 until height) {
                for (w in 0 until width) {
                    val x = floatArray[h * width * channels + w * channels + c]
                    floatBuffer.put(x)
                }
            }
        }

        // Create a TensorBuffer with new shape [1, channels, height, width]
        val newShape = intArrayOf(channels, height, width)
        val transposedBuffer = TensorBuffer.createFixedSize(newShape, buffer.dataType)
        newBuffer.rewind()  // Rewind the ByteBuffer before loading
        transposedBuffer.loadBuffer(newBuffer)

        return transposedBuffer
    }

    fun getBitmapFromDrawable(context: Context, drawableId: Int): Bitmap {
        val drawable: Drawable? = ContextCompat.getDrawable(context, drawableId)
        val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun runInference(inputBuffer: ByteBuffer) {
        // Assuming model's output is a single float array
        val outputTensor0 = FloatArray(1) { 0.0f }  // Shape: [1]
        val outputTensor1 = Array(1) { FloatArray(2) { 0.0f } }  // Shape: [1, 2]
        val outputTensor2 = Array(1) { ByteArray(3) }
        val outputs = mutableMapOf<Int, Any>(
//            0 to outputTensor0,
//            1 to outputTensor1,
            0 to outputTensor2
        )
        val outputVal = interpreter.runForMultipleInputsOutputs(arrayOf(inputBuffer), outputs)
//        val resultsTensor0 = outputs[0] as FloatArray
//        val resultsTensor1 = outputs[1] as Array<FloatArray>
        val resultsTensor2 = outputs[0] as LongArray
//        println("Output Tensor 0: ${resultsTensor0.contentToString()}")
//        println("Output Tensor 1: ${resultsTensor1.contentDeepToString()}")
        println("Output Tensor 2: ${resultsTensor2.contentToString()}")
    }
}