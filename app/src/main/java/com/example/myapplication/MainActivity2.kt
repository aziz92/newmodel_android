package com.example.myapplication

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.myapplication.databinding.ActivityMain2Binding
import android.content.Context
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.support.common.FileUtil
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.widget.ImageView
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer

class MainActivity2 : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMain2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        // Replace with your resource ID and label
        val resourceId = resources.getIdentifier("blur1", "drawable", packageName)
        val bitmap = BitmapFactory.decodeResource(resources, resourceId)
        val resizedBitmap = Bitmap.createScaledBitmap(bitmap, 896, 896, true)
        val subImages = createSubImages(resizedBitmap)

        // Get references to ImageViews
        val imageViewCenter = findViewById<ImageView>(R.id.imageViewCenter)
        val imageViewTop = findViewById<ImageView>(R.id.imageViewTop)
        val imageViewBottom = findViewById<ImageView>(R.id.imageViewBottom)
        val imageViewLeft = findViewById<ImageView>(R.id.imageViewLeft)
        val imageViewRight = findViewById<ImageView>(R.id.imageViewRight)

        // Set bitmaps to ImageViews
        imageViewCenter.setImageBitmap(subImages[0])
        imageViewTop.setImageBitmap(subImages[1])
        imageViewBottom.setImageBitmap(subImages[2])
        imageViewLeft.setImageBitmap(subImages[3])
        imageViewRight.setImageBitmap(subImages[4])


//        checkModelDetails(this)
        runInference(subImages)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main2)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    fun createSubImages(bitmap: Bitmap): List<Bitmap> {
        val subImages = mutableListOf<Bitmap>()
        val width = 224
        val height = 224

        // Center
        val centerX = (bitmap.width - width) / 2
        val centerY = (bitmap.height - height) / 2
        val centerBitmap = Bitmap.createBitmap(bitmap, centerX, centerY, width, height)
        subImages.add(centerBitmap)

        // Top
        val topX = (bitmap.width - width) / 2
        val topY = (bitmap.height / 4) - (height / 2)
        val topBitmap = Bitmap.createBitmap(bitmap, topX, topY, width, height)
        subImages.add(topBitmap)

        // Bottom
        val bottomX = (bitmap.width - width) / 2
        val bottomY = (3 * bitmap.height / 4) - (height / 2)
        val bottomBitmap = Bitmap.createBitmap(bitmap, bottomX, bottomY, width, height)
        subImages.add(bottomBitmap)

        // Left
        val leftX = (bitmap.width / 4) - (width / 2)
        val leftY = (bitmap.height - height) / 2
        val leftBitmap = Bitmap.createBitmap(bitmap, leftX, leftY, width, height)
        subImages.add(leftBitmap)

        // Right
        val rightX = (3 * bitmap.width / 4) - (width / 2)
        val rightY = (bitmap.height - height) / 2
        val rightBitmap = Bitmap.createBitmap(bitmap, rightX, rightY, width, height)
        subImages.add(rightBitmap)

        return subImages
    }



    fun runInference(bitmaps: List<Bitmap>) {
        val modelFilename = "may16.tflite"
        val tfliteModel = FileUtil.loadMappedFile(this, modelFilename)
        val interpreter = Interpreter(tfliteModel)

        // List of image resource IDs
        val imageNames = listOf("fake", "real", "blur")
        val results = mutableListOf<TensorBuffer>()

        for (bitmap in bitmaps) {
            val tensorImage = TensorImage.fromBitmap(bitmap)

            // Assuming the model's input and output shapes are known
            val inputShape = interpreter.getInputTensor(0).shape()
            val outputShape = interpreter.getOutputTensor(0).shape()

            val inputBuffer = tensorImage.tensorBuffer
            val outputBuffer = TensorBuffer.createFixedSize(outputShape, interpreter.getOutputTensor(0).dataType())

            interpreter.run(inputBuffer.buffer, outputBuffer.buffer.rewind())

            results.add(outputBuffer)
        }

        // Print the results
        for ((index, outputBuffer) in results.withIndex()) {
            println("Result for sub-image $index:")
            println(outputBuffer.floatArray.joinToString(", "))
        }

        // Close the interpreter
        interpreter.close()
    }

    fun checkModelDetails(context: Context) {
        // Load the TensorFlow Lite model from the assets folder
        val tfliteModel = FileUtil.loadMappedFile(context, "large.tflite")
        val interpreter = Interpreter(tfliteModel)

        // Get model input information
        val inputTensor = interpreter.getInputTensor(0)
        println("Input shape: ${inputTensor.shape().joinToString()}")
        println("Input data type: ${inputTensor.dataType()}")

        // Get model output information
        val outputTensor = interpreter.getOutputTensor(0)
        println("Output shape: ${outputTensor.shape().joinToString()}")
        println("Output data type: ${outputTensor.dataType()}")

        // Close the interpreter
        interpreter.close()
    }
}